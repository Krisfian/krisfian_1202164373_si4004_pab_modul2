package com.example.krisfian_120216473_si4004_pab_modul2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class Main2Activity extends AppCompatActivity {
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ((TextView)findViewById(R.id.tv)).setText("Rp. "+ NumberFormat.getNumberInstance(Locale.US).format(getIntent().getIntExtra("harga", 0)));
        ((TextView)findViewById(R.id.tj)).setText(getIntent().getStringExtra("tujuan"));
        ((TextView) findViewById(R.id.tglbrgkt)).setText(getIntent().getStringExtra("tglbrgkt"));
        ((TextView)findViewById(R.id.jumlah)).setText(getIntent().getStringExtra("jumlahtiket"));

        if (getIntent().getBooleanExtra("check_pp", false)){
            ((LinearLayout)findViewById(R.id.cektanggalpulang)).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tglplg)).setText(getIntent().getStringExtra("tglplg"));
        }
    }


    public void klik(View view) {
        int saldo = getIntent().getIntExtra("topup", 0) - getIntent().getIntExtra("harga", 0);
        Intent baru = new Intent(this, MainActivity.class);
        baru.putExtra("saldo", saldo);
        startActivity(baru);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(Main2Activity.class.getSimpleName(), "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(Main2Activity.class.getSimpleName(), "onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(Main2Activity.class.getSimpleName(), "onResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(Main2Activity.class.getSimpleName(), "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(Main2Activity.class.getSimpleName(), "onPause");
    }
}
